from django.db import models

class AreaSync(models.Model):
    name = models.CharField(max_length=100, default='')
    area = models.PositiveIntegerField()
    is_usable = models.BooleanField()
    description = models.TextField(default='', blank=True)

    class Meta:
        pass