from django.db import models

class AreaMigrate(models.Model):
    name = models.CharField(max_length=100, default='')
    area = models.PositiveIntegerField(default=400)
    is_usable = models.BooleanField(default=False)
    description = models.TextField(default='', blank=True)
    build_date = models.DateField(auto_now=True, auto_now_add=True)
    last_updated = models.DateTimeField(auto_now=True, auto_now_add=True)

    db_defaults = {'name' : '', 'area' : 400}

    class Meta:
        pass