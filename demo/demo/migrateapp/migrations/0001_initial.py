# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'AreaMigrate'
        db.create_table('migrateapp_areamigrate', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default='', max_length=100)),
            ('area', self.gf('django.db.models.fields.PositiveIntegerField')(default=400)),
            ('is_usable', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('description', self.gf('django.db.models.fields.TextField')(default='', blank=True)),
        ))
        db.send_create_signal('migrateapp', ['AreaMigrate'])


    def backwards(self, orm):
        # Deleting model 'AreaMigrate'
        db.delete_table('migrateapp_areamigrate')


    models = {
        'migrateapp.areamigrate': {
            'Meta': {'object_name': 'AreaMigrate'},
            'area': ('django.db.models.fields.PositiveIntegerField', [], {'default': '400'}),
            'description': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_usable': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'})
        }
    }

    complete_apps = ['migrateapp']