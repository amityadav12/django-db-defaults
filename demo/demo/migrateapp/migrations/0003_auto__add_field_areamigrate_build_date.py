# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'AreaMigrate.build_date'
        db.add_column(u'migrateapp_areamigrate', 'build_date',
                      self.gf('django.db.models.fields.DateField')(auto_now=True, auto_now_add=True, default=datetime.datetime(2013, 8, 27, 0, 0), blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'AreaMigrate.build_date'
        db.delete_column(u'migrateapp_areamigrate', 'build_date')


    models = {
        u'migrateapp.areamigrate': {
            'Meta': {'object_name': 'AreaMigrate'},
            'area': ('django.db.models.fields.PositiveIntegerField', [], {'default': '400'}),
            'build_date': ('django.db.models.fields.DateField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_usable': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'})
        }
    }

    complete_apps = ['migrateapp']